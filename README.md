# Inventario

Pasos para el despliegue de una web de inventario PHP – JS – MVC

Verificar módulos activos
**root@docker-vm:~# kubectl cluster-info**

En caso de que se requiera, habilitar DNS
**root@docker-vm:~# microk8s enable dns**

Nos ubicamos o creamos una carpeta para descargar el repositorio de GITLAB, para este ejemplo, utilizamos el directorio 
/root/proyecto
	
Para la descarga del repositorio
**root@docker-vm:~/proyecto# git clone https://gitlab.com/jdsantucho/inventario.git**

Del repo se descargará una carpeta con cuatro archivos en su interior 

1-	inventario.sql # Contiene la estructura de la bd

2-	mysql-deployment.yaml # Contiene comandos para el despliegue de mysql

3-	mysql-pv.yaml # Contiene comandos para la creación de volúmenes persistentes

4-	php-deployment.yaml # Contiene comandos para el despliegue de la web en php


Nos movemos a la carpeta inventario
**root@docker-vm:~/proyecto# cd inventario/**

Crear deployment php
**root@docker-vm:~/proyecto/inventario# kubectl apply -f php-deployment.yaml**

Crear pv
**root@docker-vm:~/proyecto/inventario# kubectl apply -f mysql-pv.yaml**

Crear deployment mysql
**root@docker-vm:~/proyecto/inventario# kubectl apply -f php-deployment.yaml**

Listar pods creados
**root@docker-vm:~/proyecto/inventario# kubectl get pods**

Se mostraran los PODS creados 
Ejemplo:
    POD: webapp1-656545f8cf-msdwm # Sistema PHP
    POD: sqldb-57cb9d5965-d7rhc # Base de datos

Listar volúmenes persistentes
**root@docker-vm:~/proyecto/inventario# kubectl get pv**

Listar servicios
**root@docker-vm:~/proyecto/inventario# kubectl get svc**


Al sistema php se puede ingresar con la IP externa del host que ejecuta k8s, más el puerto 31600, pero aún resta trabajar sobre la bd




En mi caso se ingresa desde 192.168.0.235:31600 

Ahora vamos a trabajar sobre el pod que contiene la bd, se utiliza el POD sqldb-57cb9d5965-d7rhc
**root@docker-vm:~/proyecto/inventario# kubectl exec -ti sqldb-57cb9d5965-d7rhc bash**

Dentro del pod, ingresamos a mysql con usuario “root”, y clave “password”
**root@sqldb-57cb9d5965-d7rhc:/# mysql -u root –p**
Enter password: # aca pongo password

Dentro de mysql, crear base de datos
**mysql> create database bd_inventario;**

Ingresar a la base creada
**mysql> use bd_inventario;**

Agregar todo el contenido de inventario.sql
https://gitlab.com/jdsantucho/inventario.git 
Archivo inventario.sql


Pegarlo el contenido en **mysql>**


Salimos de mysql

**mysql> exit**


Salimos del pod sqldb-57cb9d5965-d7rhc

**root@sqldb-57cb9d5965-d7rhc:/# exit**

Al tener un volumen persistente para la bd, no importa que el pod se destruya, el
contenido lo tendremos dentro de /mnt/data/bd_inventario/

*root@docker-vm:/mnt/data/bd_inventario# ls**

**departamento.ibd estacion.ibd gerencia.ibd impresora.ibd lspersonal.ibd marca.ibd
modelo.ibd propietario.ibd rol.ibd usuario.ibd**

Terminamos, podemos ingresar al sitio web con la IP y puerto ya mencionados para este ejemplo

http://192.168.0.235:31600/Login/index.php

Usuario: admin
Contraseña: 1234


Tuto con Word
https://drive.google.com/file/d/1nb8tcFcdAg2UjDxuUZkd2bK0JvYsv_JM/view?usp=sharing
